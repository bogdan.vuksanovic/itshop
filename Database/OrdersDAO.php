<?php
require_once 'db.php';

class DAO {
	private $db;


	private $INSERTORDER = "INSERT INTO order_details(id_detail, id_product, amount, id_order) VALUES (?, ?, ?, ?)";
	private $DELETEORDER = "DELETE FROM order_details WHERE id_detail = ?";
	private $SELECTORDERBYID = "SELECT * FROM order_details WHERE id_detail = ?";	
	private $GETORDER = "SELECT * FROM order_details ORDER BY id_detail DESC LIMIT ?";
	private $SELECTORDERS = "SELECT * FROM order_details";
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getProduct($n)
	{
		$statement = $this->db->prepare($this->GETORDER);
		$statement->bindValue(1, $n, PDO::PARAM_INT);
		$statement->execute();

		$result = $statement->fetchAll();
		return $result;
	}

	public function insertProduct($id_product, $amount, $id_order)
	{
		$statement = $this->db->prepare($this->INSERTORDER);
		$statement->bindValue(1, $id_product);
		$statement->bindValue(2, $amount);
		$statement->bindValue(3, $id_order);
		
		$statement->execute();
	}

	public function deleteProduct($id_detail)
	{
		$statement = $this->db->prepare($this->DELETEORDER);
		$statement->bindValue(1, $id_detail);
		
		$statement->execute();
	}

	public function getProductById($id_detail)
	{
		$statement = $this->db->prepare($this->SELECTORDERBYID);
		$statement->bindValue(1, $id_detail);
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function selectProducts()
	{
		$statement = $this->db->prepare($this->SELECTORDERS);
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
}
?>
