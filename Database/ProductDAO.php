<?php
require_once 'db.php';

class DAO {
	private $db;


	private $INSERTPRODUCT = "INSERT INTO products(id_product, name, type, price, active) VALUES (?, ?, ?, ?, ?)";
	private $DELETEPRODUCT = "DELETE FROM products WHERE id_product = ?";
	private $SELECTBYID = "SELECT * FROM products WHERE id_product = ?";	
	private $GETPRODUCT = "SELECT * FROM products ORDER BY id_product DESC LIMIT ?";
	private $SELECTPRODUCTS = "SELECT * FROM products";
	private $SELECTMANUFACTRURERS = "SELECT * FROM manufacturers";
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getProduct($n)
	{
		$statement = $this->db->prepare($this->GETPRODUCT);
		$statement->bindValue(1, $n, PDO::PARAM_INT);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function insertProduct($name, $type, $price, $active)
	{
		$statement = $this->db->prepare($this->INSERTPRODUCT);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $type);
		$statement->bindValue(3, $price);
		$statement->bindValue(4, $active);
		
		$statement->execute();
	}

	public function deleteProduct($id_product)
	{
		$statement = $this->db->prepare($this->DELETEPRODUCT);
		$statement->bindValue(1, $id_product);
		
		$statement->execute();
	}

	public function getProductById($id_product)
	{
		$statement = $this->db->prepare($this->SELECTBYID);
		$statement->bindValue(1, $id_product);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function selectProducts()
	{
		$statement = $this->db->prepare($this->SELECTPRODUCTS);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectManufacturers()
	{
		$statement = $this->db->prepare($this->SELECTMANUFACTRURERS);

		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
}
?>
