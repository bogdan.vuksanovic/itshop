<?php
require_once 'db.php';

class DAO {
	private $db;

	// za 2. nacin resenja
	private $SELECT_USER_BY_USERNAME_AND_PASSWORD = "";	
	private $SELECT_USER_BY_USERNAME = "";	
	private $INSERT_USER_ATRIBUTES = "INSERT INTO users_atributes(ime, prezime, email) values (?, ?, ?)";	
	private $INSERT_USER = "INSERT INTO users (type, username, password, id_user_atributes) values ('kupac', ?, ?, ?)";	
	private $SELECT_ARTICLES = "";	
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	private function insertUserAtributes($ime, $prezime, $email)
	{
		
		$statement = $this->db->prepare($this->INSERT_USER_ATRIBUTES);
		$statement->bindValue(1, $ime);
		$statement->bindValue(2, $prezime);
		$statement->bindValue(3, $email);

		
		$statement->execute();
		return $this->db->lastInsertId();
	}

	private function insertUser($username, $password, $id_user_atributes)
	{
		
		$statement = $this->db->prepare($this->INSERT_USER);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		$statement->bindValue(3, $id_user_atributes);

		
		$statement->execute();
	}

	public function selectUserByUsernameAndPassword($username, $password)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME_AND_PASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	public function selectUserByUsername($username)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME);
		$statement->bindValue(1, $username);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	public function insertUserWithAtributes($ime, $prezime, $email, $username, $password){

		$id_user_atrubutes = $this->insertUserAtributes($ime, $prezime, $email);
		$this->insertUser($username, $password, $id_user_atrubutes); 
	}

	public function selectArticles()
	{
		
		$statement = $this->db->prepare($this->SELECT_ARTICLES);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}


}
?>
