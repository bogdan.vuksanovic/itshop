<?php include_once '../partials/header.php'?>
        <div class="setup">
            <button><i class="fa fa-cog" aria-hidden="true"></i></button><br>
            <button><i class="fa fa-sliders" aria-hidden="true"></i></button><br>
        </div>

        <h3 style="margin-left: 120px;">Our Location</h3>
    <div id="map"></div>


    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg&callback=initMap&v=weekly"
      defer
    ></script>
        
        <!-- <form name="form1" action="#"> -->
        <h3>Drop Us A Line</h3>
        <p>Have a question or comment? Use the form below to send us a message or contact by mail</p>
    
        <select name="service" id="services" placeholder="Customer service">
            <option value=""></option>
            <option value=""></option>
            <option value=""></option>
            <option value=""></option>
        </select>
            <input class="email" id='email' type="email" placeholder="your@email.com" onchange='checkEmail()'>
            <input type="file" name="file" id="file" onchange="ValidateExtension()"><br>
            <span id = "lblError" style="color: red"></span>
            <textarea name="textarea" id="txt-area" cols="30" rows="10" placeholder="How can we help?"></textarea><br>
            <input class="submit" id="post" type="submit" value="Post Comment" onchange="ValidateEmail(inputText)">
        <!-- </form> -->
    <div id='email-validity'></div>
        
    
        <script src="../JS/javaScript/01.js"></script>
    
        <?php include_once '../partials/footer.php'?>
        <?php

        $errors = [];
        $data = [];

        if (empty($_POST['email'])) {
            $errors['email'] = 'Email is required.';
        }

        if (!empty($errors)) {
            $data['success'] = false;
            $data['errors'] = $errors;
        } else {
            $data['success'] = true;
            $data['message'] = 'Success!';
        }

        echo json_encode($data);
        ?>

        <script>
function checkEmail() {
const xmlhttp = new XMLHttpRequest();
xmlhttp.onload = function() {
  const myObj = JSON.parse(this.responseText);
  document.getElementById("email-validity").innerHTML = myObj.valid;
}
xmlhttp.open("GET", "email-check.php?email=" + document.getElementById('email').value);
xmlhttp.send();
}
        </script>