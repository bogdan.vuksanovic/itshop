<?php 
require_once '../Database/ProductDAO.php';

$dao = new DAO();
$manufacturers = $dao->selectManufacturers();?>

<?php include_once '../partials/header.php'?>
content 

<link rel="stylesheet" href="javaScript">
<div id='products'></div><br>
<form action="">
<?php foreach($manufacturers as $pom){?>
    <input type="checkbox"><?= $pom['manufacturer']?><br>
<?php }?>
</form>
<?php include_once '../partials/footer.php'?>


<?php

$productsLIst = [
    [
        'name' => 'Samsung 123',
        'type' => 'mobile',
        'active' => false,
    ],
    [
        'name' => 'Iphone 21',
        'type' => 'web',
        'active' => true,
    ],
    [
        'name' => 'Huawei',
        'type' => 'web',
        'active' => true,
    ],
    [
        'name' => 'Honor',
        'type' => 'web',
        'active' => false,
    ],
];

// ?>

<script>


let productsList = <?php echo json_encode($productsLIst); ?>

var productsHtml = '';

for (let index = 0; index < productsList.length; index++) {
    if (productsList[index].active) {

        productsHtml += productsList[index].name + ' </br>';
    }
}

document.getElementById('products').innerHTML = productsHtml;

</script>
