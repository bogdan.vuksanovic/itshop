    <?php include_once '../partials/header.php'?>
    <link rel="stylesheet" href="../css/stil2.css">
        <div class="container-main">
            <div class="article1">
                <div class="text1">
                    <div class="small-txt">
                        <p>MIDWEEK DEALS</p>
                    </div>
                    <h1>SMARTPHONES ACCESSORIES</h1>
                    <p>Up to 40% off</p>
                    <button class="shop-now">Shop Now <i class="fa fa-arrow-right"></i></button>
                </div>
                    <img src="../pictures/Social icons (349).png" alt="">
            </div>
            <div class="article2">
                <div class="article21">
                    <div class="text1">
                        <div class="small-txt">
                            <p>LIMITED STOCK</p>
                        </div>
                        <p class="mac">MACBOOK AIR</p>
                        <p>starting from <div class="price">$900.99</div></p>
                        <a href="">Shop Now <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <img src="../pictures/Social icons (349).png" alt="">
                </div>
                <div class="article22">
                    <div class="text1">
                        <div class="small-txt">
                            <p>DISCOVER</p>
                        </div>
                        <p class="mac">BEAT PRO X3</p>
                        <p>$259.99 <div class="price">$199.99</div></p>
                        <a href="">Shop Now <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <img src="../pictures/Social icons (349).png" alt="">
                </div>
            </div>
        </div>
        <h2>Our Featured Product</h2>

        <div class="container-product">
            <div class="row">
            
                <div class="col-1">
                    <img src="../pictures/Social icons (349).png" alt="">
                    <h4>Samsung Galaxy M51</h4>
                    <div class="rating">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <p>$19.12</p>
                </div>
                <div class="col-1">
                    <img src="../pictures/Social icons (349).png" alt="">
                    <h4>Lenovo IdeaPad S540 13.3"</h4>
                    <div class="rating">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <p>$28.72</p>
                </div>
                <div class="col-1">
                    <img src="../pictures/Social icons (349).png" alt="">
                    <h4>TV LED LG Full HD 43 Inch</h4>
                    <div class="rating">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <p>$27.55</p>
                </div>
                <div class="col-1">
                    <img src="../pictures/Social icons (349).png" alt="">
                    <h4>HP Pavilion</h4>
                    <div class="rating">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <p>$29.00</p>
                </div>
            </div>
        </div>
        <div class="week-deals">
            <h2>This Week Deals</h2>
            <p><i class="fa fa-clock-o"></i><b>307</b>days:<b>15</b>hours:<b>33</b>min</p>
        </div>
        <div class="container-product">
            <div class="row">
        
        <div class="col-3">
            <img src="../pictures/Social icons (349).png" alt="">
            <h4>Samsung Galaxy M51</h4>
            <div class="rating">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
            </div>
            <p>$19012</p>
        </div>
        <div class="col-3">
            <img src="../pictures/Social icons (349).png" alt="">
            <h4>Lenovo IdeaPad S540</h4>
            <div class="rating">
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
                <i class="fa fa-star-o"></i>
            </div>
            <p>$28.72</p>
        </div>
    </div>
</div>
<?php include_once '../partials/footer.php'?>

<?php

$productsList = [
    [
        'name' => 'Samsung 123',
        'type' => 'mobile',
        'active' => false,
    ],
    [
        'name' => 'Iphone 21',
        'type' => 'web',
        'active' => true,
    ],
    [
        'name' => 'Huawei',
        'type' => 'web',
        'active' => true,
    ],
    [
        'name' => 'Honor',
        'type' => 'web',
        'active' => 'false',
    ],
];

// ?>

<script>


let productsList = <?php echo json_encode($productsList); ?>

var productsHtml = '';

for (let index = 0; index < productsList.length; index++) {
    if (productsList[index].active) {

        productsHtml += productsList[index].name + ' </br>';
    }
}

document.getElementById('products-list').innerHTML = productsHtml;

</script>

