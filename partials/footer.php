<link rel="stylesheet" href="../css/stil2.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="javaScript/01.js"></script>
</main>
    <footer>
        <div class="brands">
            <div class="small-container">
                <div class="row">
                    <div class="col-2">
                        <img src="../pictures/Fanta.png" alt="">
                    </div>
                    <div class="col-2">
                        <img src="../pictures/Fanta.png" alt="">
                    </div>
                    <div class="col-2">
                        <img src="../pictures/Fanta.png" alt="">
                    </div>
                    <div class="col-2">
                        <img src="../pictures/Fanta.png" alt="">
                    </div>
                    <div class="col-2">
                        <img src="../pictures/Fanta.png" alt="">
                    </div>
                    <div class="col-2">
                        <img src="../pictures/Fanta.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-footer">
            <div class="message">
                
                <h3><i class="fa fa-envelope"></i>Fear Of Missing Out?</h3>
                <p>Get the latest deals, updates and more</p>
            </div>
            <div class="subscribe">
                <input type="text" >
                <div class="submit">
                <input type="submit" value="Subscribe" style="background:red">
            </div>
            </div>
            <div class="social-media">
                <p>Follow us</p>
                <p><i class="fa fa-facebook"></i></p>
                <p><i class="fa fa-twitter"></i></p>
                <p><i class="fa fa-instagram"></i></p>
                <p><i class="fa fa-youtube-play"></i></p>

            </div>
        </div>
        <div class="container-about">
            <div class="company">
                <h3>COMPANY</h3>
                <ul>
                    <li><a href="">About Us</a></li>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Affilates</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contact Us</a></li>
                </ul>
            </div>
            <div class="shop">
                <h3>SHOP</h3>
                <ul>
                    <li><a href="">Televisions</a></li>
                    <li><a href="">Fridges</a></li>
                    <li><a href="">Washing Machines</a></li>
                    <li><a href="">Fans</a></li>
                    <li><a href="">Air Conditioners</a></li>
                    <li><a href="">Laptops</a></li>
                </ul>
            </div>
            <div class="help">
                <h3>HELP</h3>
                <ul>
                    <li><a href="">Customers Service</a></li>
                    <li><a href="">My Account</a></li>
                    <li><a href="">Find a Store</a></li>
                    <li><a href="">Legal & Privacy</a></li>
                    <li><a href="">Contact</a></li>
                    <li><a href="">Gidft Card</a></li>
                </ul>
            </div>
            <div class="account">
                <h3>MY ACCOUNT</h3>
                <ul>
                    <li><a href="">My Profile</a></li>
                    <li><a href="">My Order History</a></li>
                    <li><a href="">My Wish List</a></li>
                    <li><a href="">Order Tracking</a></li>
                </ul>
            </div>
            <div class="contact1">
                <h3>CONTACT INFO</h3>
                <p class="num">(+86)-01234-5678</p>
                <p>Mon to Sun:10.00 AM to 6.00 PM</p>
                <p class="num">demo@demo</p>
                <hr>
                <p>48West Temple Drive</p>
                <p>Ashburn, VA 20147</p>
            </div>
        </div>
        <hr>
        <p class="copyright">Copyright 2022 Technogy.All rights reserved</p>
    </footer>
    
</body>
</html>