<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header>
        <div class="container-nav">
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="#"> Shipping</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#"> Contact</a></li>
                        <li><a href="#"> Trackorder</a></li>
                    </ul>
                </nav>
                <div class="txt-nav">
                    <p>Free 30 Day Money Back Guarantee</p>
                    <p>USD</p>
                    <p>English</p>
                </div>
            </div>
        </div>
        <div class="container-logo">
            <div class="logo">
                <img src="../pictures/Android.png" alt="">
            </div>
            <div class="mail">
                <p>Send us a message</p>
                <p>demo@demo.com</p>
            </div>
            <div class="contact">
                <p>Need help?Call us</p>
                <p>012 345 6789</p>
            </div>
            
            <div class="icon">
                <i class="fa fa-address-book-o"></i>
                <i class="fa fa-heart-o"></i>
                <i class="fa fa-shopping-basket"></i>
            </div>
        </div>
        <hr>
        <div class="links">
            <button class="browse">BROWSE CATEGORIES <i class="fa fa-align-right"></i></button>
            <ul>
                <li><a href="../pages/home-page.php" class="home">HOME <i class="fa fa-arrow-down"></i></a></li>
                <li><a href="../pages/content.php">SHOP<i class="fa fa-arrow-down"></i></a></li>
                <li><a href="">PAGES<i class="fa fa-arrow-down"></i></a></li>
                <li><a href="">BLOGS</a></li>
                <li><a href="../pages/contact.php">CONTACTS</a></li>
            </ul>
            <div class="search">
                <input type="text">
                <input type="submit" value="search">
            </div>
        </div>
    </header>
    <main>