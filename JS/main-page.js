function ValidateExtension() {
    var allowedFiles = [".doc", ".docx", ".pdf"];
    var fileUpload = document.getElementById("file");
    var lblError = document.getElementById("lblError");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
    if (!regex.test(fileUpload.value.toLowerCase())) {
        lblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
        return false;
    }
    lblError.innerHTML = "";
    return true;
}
function ValidateEmail(inputText)
{
var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
if(inputText.value.match(mailformat))
{
alert("Valid email address!");
document.form1.text1.focus();
return true;
}
else
{
alert("You have entered an invalid email address!");
document.form1.text1.focus();
return false;
}
}

// Initialize and add the map
function initMap() {
    // The location of Uluru
    const uluru = { lat: 44.787197, lng: 20.457273 };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 4,
      center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
      position: uluru,
      map: map,
    });
  }
  
  window.initMap = initMap;

  let products = ["HP","Lenovo","Apple","Dell"];

  let productsList  = [
    {
      "color": "black",
      "type": "phone",
      "name": "Lenovo",
      "capacityOfMemory": "128GB"
    },
    {
      "color": "red",
      "type": "phone",
      "name": "Apple",
      "capacityOfMemory": "128GB"
    }
  ];
  


  var productsHtml = '';

for (let index = 0; index < productsList.length; index++) {
    productsHtml += productsList[index].name;
}

document.getElementById('products').innerHTML = productsHtml;
debugger

//validate email
$(document).ready(function () {
  $("form").submit(function (event) {
    var formData = {
      email: $("#email").val(),
    };

    $.ajax({
      type: "POST",
      url: "process.php",
      data: formData,
      dataType: "json",
      encode: true,
    }).done(function (data) {
      console.log(data);
    });

    event.preventDefault();
  });
});




  